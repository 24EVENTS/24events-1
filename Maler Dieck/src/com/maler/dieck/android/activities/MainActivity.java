package com.maler.dieck.android.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.app.ActionBar.TabListener;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.maler.dieck.android.R;
import com.maler.dieck.android.fragments.GalleryFragment;
import com.maler.dieck.android.fragments.WebViewFragment;

public class MainActivity extends SherlockFragmentActivity implements
		TabListener, OnPageChangeListener {

	private ViewPager mViewPager;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getSupportActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		setContentView(R.layout.main);
		mViewPager = (ViewPager) findViewById(R.id.viewpager);
		mViewPager.setOffscreenPageLimit(3);
		mViewPager.setOnPageChangeListener(this);
		mViewPager.setAdapter(new MyAdapter());
		setupTabs();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		try {
			getSupportMenuInflater().inflate(R.menu.main, menu);
			return true;
		} catch (Exception e) {
			Log.e(getClass().getSimpleName(), "Error inflating menu", e);
			return false;
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.share) {
			Intent intent = new Intent(Intent.ACTION_SEND);
			intent.setType("text/plain");
			intent.putExtra(Intent.EXTRA_TEXT,
					"http://play.google.com/store/apps/details?id="
							+ getPackageName());
			startActivity(Intent.createChooser(intent,
					getString(R.string.share)));
			return true;
		} else if (item.getItemId() == R.id.exit) {
			finish();
		}
		return super.onOptionsItemSelected(item);
	}

	private void setupTabs() {
		getSupportActionBar().addTab(
				getSupportActionBar().newTab().setText(R.string.tab1_title)
						.setTabListener(this));
		getSupportActionBar().addTab(
				getSupportActionBar().newTab().setText(R.string.tab2_title)
						.setTabListener(this));
		getSupportActionBar().addTab(
				getSupportActionBar().newTab().setText(R.string.tab4_title)
						.setTabListener(this));
		getSupportActionBar().addTab(
				getSupportActionBar().newTab().setText(R.string.tab3_title)
						.setTabListener(this));
	}

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		mViewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
	}

	@Override
	public void onPageScrollStateChanged(int arg0) {
	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {
	}

	@Override
	public void onPageSelected(int arg0) {
		getSupportActionBar().setSelectedNavigationItem(arg0);
	}

	private class MyAdapter extends FragmentPagerAdapter {

		public MyAdapter() {
			super(getSupportFragmentManager());
		}

		@Override
		public Fragment getItem(int arg0) {
			if (arg0 == 0) {
				Fragment fragment = new WebViewFragment();
				Bundle args = new Bundle();
				args.putString("file", "tab1.html");
				fragment.setArguments(args);
				return fragment;
			} else if (arg0 == 1) {
				Fragment fragment = new WebViewFragment();
				Bundle args = new Bundle();
				args.putString("file", "tab2.html");
				fragment.setArguments(args);
				return fragment;
			} else if (arg0 == 3) {
				Fragment fragment = new WebViewFragment();
				Bundle args = new Bundle();
				args.putString("file", "tab3.html");
				fragment.setArguments(args);
				return fragment;
			} else if (arg0 == 2) {
				return new GalleryFragment();
			} else {
				return null;
			}
		}

		@Override
		public int getCount() {
			return 4;
		}
	}

}