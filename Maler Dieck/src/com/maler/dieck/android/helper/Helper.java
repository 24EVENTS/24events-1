package com.maler.dieck.android.helper;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import android.util.Log;

public class Helper {

	public static String readToEnd(InputStream in) {
		try {
			byte[] buffer = new byte[1024];
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			int count = 0;
			while ((count = in.read(buffer)) != -1) {
				baos.write(buffer, 0, count);
			}
			in.close();
			return baos.toString("UTF-8");
		} catch (Exception e) {
			Log.e(Helper.class.getSimpleName(), "Error reading", e);
			return "Error";
		}
	}

}
