package com04131.android.lib;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.*;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

public class Helper {

    public static void storeEvents(Context context, List<Event> events)
            throws Exception {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);
        oos.writeObject(events);
        String data = Helper.base64_encode(baos.toByteArray());
        PreferenceManager.getDefaultSharedPreferences(context).edit()
                .putString(AppWidgetProvider.PREF_DATA, data).commit();
        oos.close();
    }

    @SuppressWarnings("unchecked")
    public static List<Event> loadEvents(Context context) throws Exception {
        String base64 = PreferenceManager.getDefaultSharedPreferences(context)
                .getString(AppWidgetProvider.PREF_DATA, null);
        byte[] data = Helper.base64_decode(base64);
        ByteArrayInputStream bais = new ByteArrayInputStream(data);
        ObjectInputStream ois = new ObjectInputStream(bais);
        List<Event> events = (List<Event>) ois.readObject();
        ois.close();
        return events;
    }

    public static void updateWidget(Context context, boolean loading) {
        SharedPreferences prefs = PreferenceManager
                .getDefaultSharedPreferences(context);
        AppWidgetManager appWidgetManager = AppWidgetManager
                .getInstance(context);
        try {
            List<Event> events = Helper.loadEvents(context);
            int currentPosition = prefs.getInt(AppWidgetProvider.PREF_POSITION,
                    0);
            RemoteViews views = new RemoteViews(context.getPackageName(),
                    R.layout.widget);
            if (events != null && currentPosition < events.size()) {
                views.setTextViewText(R.id.location,
                        events.get(currentPosition).venue);
                views.setTextViewText(R.id.title,
                        events.get(currentPosition).title);
                GregorianCalendar cal = new GregorianCalendar();
                cal.setTime(events.get(currentPosition).startDate);
                views.setTextViewText(
                        R.id.start_date,
                        String.format("%02d.%02d.",
                                cal.get(Calendar.DAY_OF_MONTH),
                                cal.get(Calendar.MONTH) + 1));
                cal.setTime(events.get(currentPosition).startTime);
                views.setTextViewText(
                        R.id.start_time,
                        String.format("%02d:%02d",
                                cal.get(Calendar.HOUR_OF_DAY),
                                cal.get(Calendar.MINUTE)));
                views.setBoolean(R.id.back_wrapper, "setEnabled",
                        currentPosition > 0);
                views.setBoolean(R.id.forward_wrapper, "setEnabled",
                        currentPosition < events.size() - 1);
                // Intent contentIntent = new Intent(context,
                // MainActivity.class);
                // if (events != null && currentPosition < events.size()) {
                // Log.d(context.getPackageName(),
                // "Setting url2 on pending intent");
                // contentIntent
                // .putExtra("event", events.get(currentPosition));
                // }
                views.setOnClickPendingIntent(R.id.content, PendingIntent
                        .getActivity(context,
                                AppWidgetProvider.REQUEST_CONTENT, new Intent(
                                context, MainActionBarActivity.class)
                                .putExtra("event",
                                        events.get(currentPosition)),
                                PendingIntent.FLAG_UPDATE_CURRENT));
            }

            if (loading) {
                views.setViewVisibility(R.id.content, View.GONE);
                views.setViewVisibility(R.id.loading, View.VISIBLE);
                views.setViewVisibility(R.id.refresh_wrapper, View.GONE);
            } else if (events != null) {
                views.setViewVisibility(R.id.content, View.VISIBLE);
                views.setViewVisibility(R.id.loading, View.GONE);
                views.setViewVisibility(R.id.refresh_wrapper, View.GONE);
            } else {
                views.setViewVisibility(R.id.content, View.GONE);
                views.setViewVisibility(R.id.refresh_wrapper, View.VISIBLE);
                views.setViewVisibility(R.id.loading, View.GONE);
            }

            views.setOnClickPendingIntent(R.id.back_wrapper, PendingIntent
                    .getBroadcast(
                            context,
                            AppWidgetProvider.REQUEST_BACK,
                            new Intent(Helper.getIntentAction(context,
                                    INTENT_ACTIONS.BACK)), 0));
            views.setOnClickPendingIntent(R.id.forward_wrapper, PendingIntent
                    .getBroadcast(
                            context,
                            AppWidgetProvider.REQUEST_FORWARD,
                            new Intent(Helper.getIntentAction(context,
                                    INTENT_ACTIONS.FORWARD)), 0));
            views.setOnClickPendingIntent(R.id.widget_title, PendingIntent
                    .getActivity(
                            context,
                            AppWidgetProvider.REQUEST_TITLE,
                            new Intent(Intent.ACTION_VIEW, Uri.parse(context
                                    .getString(R.string.url_widget_title))), 0));
            views.setOnClickPendingIntent(R.id.widget_icon,
                    PendingIntent.getActivity(context,
                            AppWidgetProvider.REQUEST_ICON, new Intent(context,
                            MainActionBarActivity.class), 0));
            views.setOnClickPendingIntent(R.id.refresh, PendingIntent
                    .getService(context, AppWidgetProvider.REQUEST_REFRESH,
                            new Intent(context, UpdateWidgetService.class), 0));
            try {
                appWidgetManager.updateAppWidget(new ComponentName(context, AppWidgetProvider.class), views);
                appWidgetManager.updateAppWidget(new ComponentName(context,
                        context.getPackageName() + ".AppWidgetProvider"), views);
            } catch (Exception e) {
                Log.e(Helper.class.getSimpleName(),
                        "Error getting widget provider class", e);
            }
        } catch (Exception e) {
            RemoteViews views = new RemoteViews(context.getPackageName(),
                    R.layout.widget);
            views.setViewVisibility(R.id.content, View.GONE);
            views.setViewVisibility(R.id.loading, View.GONE);
            views.setViewVisibility(R.id.refresh_wrapper, View.VISIBLE);
            views.setOnClickPendingIntent(R.id.refresh, PendingIntent
                    .getService(context, AppWidgetProvider.REQUEST_REFRESH,
                            new Intent(context, UpdateWidgetService.class), 0));
            try {
                appWidgetManager.updateAppWidget(
                        new ComponentName(context, Class.forName(context
                                .getPackageName() + ".AppWidgetProvider")),
                        views);
            } catch (Exception e1) {
                Log.e(Helper.class.getSimpleName(),
                        "Error getting widget provider class", e1);
            }
            // appWidgetManager.updateAppWidget(new ComponentName(context,
            // Helper
            // .getAppWidgetProviderClass(context)), views);
            Log.e(context.getPackageName(), "Error updating widget", e);
        }
    }

    @SuppressWarnings("unchecked")
    public static byte[] base64_decode(String data) throws Exception {
        Class base64Class = Class
                .forName("org.apache.commons.codec.binary.Base64");
        Method decodeMethod = base64Class.getMethod("decodeBase64",
                new Class[]{byte[].class});
        byte[] bytes = (byte[]) decodeMethod.invoke(base64Class,
                data.getBytes());
        return bytes;
    }

    @SuppressWarnings("unchecked")
    public static String base64_encode(byte[] data) throws Exception {
        Class base64Class = Class
                .forName("org.apache.commons.codec.binary.Base64");
        Method encodeMethod = base64Class.getMethod("encodeBase64",
                new Class[]{byte[].class});
        return new String((byte[]) encodeMethod.invoke(base64Class, data));
    }

    public static enum INTENT_ACTIONS {
        BACK, FORWARD
    }

    ;

    public static String getIntentAction(Context context, INTENT_ACTIONS action) {
        if (action == INTENT_ACTIONS.BACK) {
            return context.getPackageName() + ".ACTION_BACK";
        } else if (action == INTENT_ACTIONS.FORWARD) {
            return context.getPackageName() + ".ACTION_FORWARD";
        } else {
            return null;
        }
    }

    public static List<Event> getEvents(Context context) {
        try {
            List<Event> events = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create().fromJson(
                    new InputStreamReader(new URL(context.getString(R.string.url_events))
                            .openStream()), new TypeToken<List<Event>>() {
            }.getType());
            Log.d(Helper.class.getSimpleName(), "Found " + (events == null ? 0 : events.size()) + " events");
            return events;
        } catch (Exception e) {
            Log.e(Helper.class.getSimpleName(), "Error loading events", e);
            return new ArrayList<Event>();
        }
    }

    // @SuppressWarnings("unchecked")
    // public static Class getAppWidgetProviderClass(Context context) {
    // try {
    // return Class.forName(context.getPackageName()
    // + ".AppWidgetProvider");
    // } catch (Exception e) {
    // Log.e(context.getPackageName(),
    // "Error getting app widget provider class", e);
    // return null;
    // }
    // }

}
