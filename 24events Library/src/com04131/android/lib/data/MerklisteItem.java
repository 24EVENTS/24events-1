package com04131.android.lib.data;

import com.codeslap.persistence.PrimaryKey;

import java.io.Serializable;

/**
 * User: Goddchen
 * Date: 16.04.13
 */
public class MerklisteItem implements Serializable {

    @PrimaryKey(autoincrement = false)
    public long id;

    public String title;

    public long created;

}
