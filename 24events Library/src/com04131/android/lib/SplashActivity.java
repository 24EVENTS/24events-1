package com04131.android.lib;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class SplashActivity extends Activity {

	private boolean mCancelled = false;

	@Override
	protected void onDestroy() {
		super.onDestroy();
		mCancelled = true;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);
		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				if (!mCancelled) {
					startActivity(new Intent(getApplicationContext(),
							MainActionBarActivity.class));
					finish();
				}
			}
		}, 2000);
	}

}
